package com.fpt.demospring.controllers;

//import com.fpt.demospring.models.Customer;
//import com.fpt.demospring.repositories.CustomerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.core.env.Environment;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.*;

@Controller
@RequestMapping(path = "")
public class CustomerController {
//    @Autowired
//    private CustomerRepository customerRepository;

    @Autowired
    private Environment env;

//    @GetMapping(path="/customer")
//    public String getAllUsers(Model model) {
//        Iterable<Customer> listCustomer = customerRepository.findAll();
//        Iterator<Customer> iterListCustomer = listCustomer.iterator();
//        List<Customer> list = new ArrayList<>();
//        iterListCustomer.forEachRemaining(list::add);
//
//        model.addAttribute("customers", list);
//
//        return "customer";
//    }

    /*
            <th>TABLE_CATALOG</th>
        <th>TABLE_SCHEMA</th>
        <th>TABLE_NAME</th>
        <th>TABLE_TYPE</th>
     */

    @GetMapping(path="/db-tables")
    public String getAllTables(Model model) {
        ArrayList output = new ArrayList< Map<String, String>>();
        try {
            String url = env.getProperty("spring.datasource.url");
            Connection conn = DriverManager.getConnection(url,env.getProperty("spring.datasource.username"),env.getProperty("spring.datasource.password"));
            Statement stmt = conn.createStatement();
            ResultSet rs;

            rs = stmt.executeQuery("SELECT * FROM INFORMATION_SCHEMA.TABLES;");
            while ( rs.next() ) {
                HashMap mMap = new HashMap();
                mMap.put("TABLE_CATALOG", rs.getString("TABLE_CATALOG"));
                mMap.put("TABLE_SCHEMA", rs.getString("TABLE_SCHEMA"));
                mMap.put("TABLE_NAME", rs.getString("TABLE_NAME"));
                mMap.put("TABLE_TYPE", rs.getString("TABLE_TYPE"));
                output.add(mMap);
            }
            conn.close();
        } catch (Exception e) {
            System.err.println("Got an exception! ");
            System.err.println(e.getMessage());
        }

        model.addAttribute("tables", output);

//        try {
//            String url = "jdbc:msql://200.210.220.1:1114/Demo";
//            Connection conn = DriverManager.getConnection(url,"","");
//            Statement stmt = conn.createStatement();
//            ResultSet rs;
//
//            rs = stmt.executeQuery("SELECT Lname FROM Customers WHERE Snum = 2001");
//            while ( rs.next() ) {
//                String lastName = rs.getString("Lname");
//                System.out.println(lastName);
//            }
//            conn.close();
//        } catch (Exception e) {
//            System.err.println("Got an exception! ");
//            System.err.println(e.getMessage());
//        }

        return "db-tables";
    }

}
